﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
using Ninject;
using ParkingMaster.Implementations;
using ParkingMaster.Interfaces;
using ParkingMaster.Service;

namespace ParkingMaster
{
    public class Message
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string MessageText { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
           // if(File.Exists(@"MyData.db") == false)
        //    {
                using (var db = new LiteDatabase(@"MyData.db"))
                {
                    var customers = db.GetCollection<Message>("customers");

                    var customer = new Message()
                    {
                        Number = "BH3456AA",
                        MessageText = " Добрый день"
                    };
                    customers.Insert(customer);
                }
          //  }


            StandardKernel kernel = new StandardKernel();
            kernel.Bind<ICamera>().To<Camera>();
            kernel.Bind<IDisplay>().To<Display>();
            kernel.Bind<INumberMessage>().To<NumberMessageDatabase>();

            kernel.Get<ShowMessageService>().Start();

            Console.ReadLine();
        }
    }
}
