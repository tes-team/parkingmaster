﻿using Ninject;
using ParkingMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMaster.Service
{
    class ShowMessageService
    {
        [Inject]
        public ICamera Camera { get; set; }
        [Inject]
        public INumberMessage NumberMessage { get; set; }
        [Inject]
        public INumberMessage NumberMessageDatabase { get; set; }
        [Inject]
        public IDisplay Display { get; set; }
 
        

        public void Start()
        {
            CarNumber carNumber = new CarNumber();
            var number = carNumber.FindNumber(Camera.GetImg("Путь к фото"));
            var message = NumberMessage.GetMessage(number);
            Display.ShowMessage(message);
        }

    }
}
