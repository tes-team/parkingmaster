﻿using ParkingMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMaster.Implementations
{
    class Display : IDisplay
    {
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
