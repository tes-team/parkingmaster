﻿using LiteDB;
using ParkingMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMaster.Implementations
{

    class NumberMessageDatabase : INumberMessage
    {
        public string GetMessage(string number)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var customers = db.GetCollection<Message>("customers");
                var results = customers.FindAll().ToList();
                if (results.Count() > 0)
                {
                    return results[0].MessageText;
                }
                else
                {
                    return "null";
                }
            }
        }
    }
}
