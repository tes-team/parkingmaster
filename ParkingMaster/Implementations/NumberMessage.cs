﻿using ParkingMaster.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMaster.Implementations
{
    class NumberMessage : INumberMessage
    {
        public string GetMessage(string number)
        {
            return number;
        }

    }
}
