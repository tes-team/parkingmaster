﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMaster.Interfaces
{
    interface INumberMessage
    {
        //Интерфейс для получения сообщения по номеру машины
        string GetMessage(string number);
    }
}
