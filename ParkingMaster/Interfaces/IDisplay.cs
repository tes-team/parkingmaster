﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMaster.Interfaces
{
    interface IDisplay
    {
        void ShowMessage(string message);
    }
}
