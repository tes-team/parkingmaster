﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingMaster.Interfaces
{
    interface ICamera
    {
        string GetImg(string img);
    }
}
