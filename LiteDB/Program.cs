﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteDB
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string[] Phones { get; set; }
        public bool IsActive { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var customers = db.GetCollection<Customer>("customers");
                var customer = new Customer
                {
                    Name = "John Doe",
                    Phones = new string[] { "8000-0000", "9000-0000" },
                    IsActive = true
                };
                customers.Insert(customer);
                customer.Name = "Joana Doe";

                customers.Update(customer);
            }

            using (var db = new LiteDatabase(@"MyData.db"))
            {
                var customers = db.GetCollection<Customer>("customers");
                var results = customers.Find(x => x.Name.Contains("a")).ToList();
              //  var results = customers.Find(x => x.Name.StartsWith("Joa")).ToList();
             //   var results = customers.Find(x => x.Name == "Joana Doe").ToList(); ;


                Console.WriteLine(results.Count);

                Console.ReadLine();
            }

        }
    }
}
